2020.3
------
Improve icons                 - Mayankmethad
Add Bluetooth Armory          - @yesimxev
Python3 incompatibility fixes - @re4son
Deauth bugfixes               - @re4son

2020.2
------
* Migrate to python3
* Fix DeAuth
* Fix VNC setup with old su binaries
* Add custom commands to start/stop wlan0 monitor mode
* Add custom commands to set USB gadget mode
* minor bug fixes
* Add workaround for Android scoped storage
* Add USB-Arsenal fragment for HID support of previously unsupported devices - @simonpunk
* Add "COMBO" command to duckyconverter - @yesimxev
* Add background location support for Android 10
* Considerable re-write by @simonpunk
* KeX manager by @yesimxev
* usbarmory script by @simonpunk
* Adds multi-user functionality to KeX manager - @yesimxev
! Multi-user KeX requires rootfs 2020.1 or newer
